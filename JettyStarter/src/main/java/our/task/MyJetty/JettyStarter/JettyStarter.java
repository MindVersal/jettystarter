/**
 * 
 */
package our.task.MyJetty.JettyStarter;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * @author isp_pia
 *
 */
public class JettyStarter {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
        Frontend frontend = new Frontend();

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(new ServletHolder(frontend), "/authform");

        Server server = new Server(8080);
        server.setHandler(context);

        server.start();
        server.join();
	}

}
